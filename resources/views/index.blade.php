<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Latihan laravel</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
    <h3>Data Pegawai</h3>

        <a href="/pegawai/tambah">+ Tambah pegawai</a>

        <br><br>

        <table class="table table-hover">
            <tr>
                <th>Nama</th>
                <th>Jabatan</th>
                <th>Umur</th>
                <th>Alamat</th>
                <th>Opsi</th>
            </tr>

            @foreach($pegawai as $p)
            <tr class="data_nya">
                <td>{{ $p->pegawai_nama }}</td>
                <td>{{ $p->pegawai_jabatan }}</td>
                <td>{{ $p->pegawai_umur }}</td>
                <td>{{ $p->pegawai_alamat }}</td>

                <td>
                    <a href="/pegawai/edit/{{ $p->pegawai_id }}">Edit</a> |
                    <!-- <a href="/pegawai/hapus/{{ $p->pegawai_id }}" id="bt-hapus" data="{{ $p->pegawai_id }}" data-bs-toggle="modal" data-bs-target="#myModal">Delete</a> -->
                    
                    <button id="btn-hapus" type="button" class="btn btn-info" data-id="{{ $p->pegawai_id }}" data-nama="{{ $p->pegawai_nama }}">Hapus</button>
                </td>
            </tr>
            @endforeach

        </table>

        <!-- Modal -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Hapus Pegawai</h4>
                </div>
                <div class="modal-body">
                <p>Data ini akan dihapus?</p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
            
            </div>
        </div>


    </div>


    <script>
        $(function (){
            $("#btn-hapus").on('click', function() {
                $("#myModal").modal('show')

                let data = $("#btn-hapus").attr('data-id');
                console.log(data);
            })
        })
    </script>

</body>
</html>