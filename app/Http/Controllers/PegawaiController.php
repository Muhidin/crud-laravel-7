<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PegawaiController extends Controller
{
    //
    public function index()
    {
        # code...
        $pegawai = DB::table('pegawai')->get();

        return view('index', ['pegawai' => $pegawai]);
    }

    public function tambah()
    {
        # code...
        $data['title'] = "Tambah Pegawai";
        return view('tambah', $data);
    }

    public function store(Request $request)
    {
        # code...
        DB::table('pegawai')->insert([
            'pegawai_nama' => $request->nama,
            'pegawai_jabatan' => $request->jabatan,
            'pegawai_umur' => $request->umur,
            'pegawai_alamat' => $request->alamat
        ]);

        return redirect('/pegawai');
        
    }

    public function edit($id)
    {
        # code...
        $data['title'] = "Edit Pegawai";
        $pegawai = DB::table('pegawai')->where('pegawai_id', $id)->get();

        return view('edit', ['pegawai' => $pegawai], $data);
    }

    public function delete($id)
    {
        # code...
        $data['title'] = "Hapus Pegawai";
        $pegawai = DB::table('pegawai')->where('pegawai_id', $id)->get();

        return view('hapus', ['pegawai' => $pegawai], $data);
    }

    public function update(Request $request)
    {
        # code...
        DB::table('pegawai')->where('pegawai_id', $request->id)->update([
            'pegawai_nama' => $request->nama,
            'pegawai_jabatan' => $request->jabatan,
            'pegawai_umur' => $request->umur,
            'pegawai_alamat' => $request->alamat
        ]);

        return redirect('/pegawai');
        
    }

}
